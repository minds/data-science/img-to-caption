##################
### Elastic IP ###
##################

resource "aws_eip" "bastion" {
  instance = aws_instance.bastion.id
  vpc      = true
}

####################
### EC2 Instance ###
####################

resource "aws_instance" "bastion" {

  // In addition to default_tags
  tags = { Name = "img-to-caption-bastion" }

  // Compute
  ami                  = var.aws_ec2_bastion_ami
  instance_type        = var.aws_ec2_bastion_instance_type
  iam_instance_profile = aws_iam_instance_profile.img_to_caption.name
  user_data            = file("${path.module}/etc/user_data_bastion.sh")

  root_block_device { volume_size = var.aws_ec2_bastion_root_volume_size /* GB */ }

  // Network
  vpc_security_group_ids = [aws_security_group.bastion.id]
  subnet_id              = var.aws_bastion_subnet_id

  // Wait for cloud-init to finish
  provisioner "remote-exec" {
    inline = ["cloud-init status --wait"]
    connection {
      type  = "ssh"
      user  = "ubuntu"
      host  = self.public_dns
      agent = true // Use local SSH agent
    }
  }
}

##############
### AWS SG ###
##############

resource "aws_security_group" "bastion" {
  name   = "img-to-caption-bastion"
  vpc_id = var.aws_vpc_id

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${var.allow_ssh_from_ip}/32"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}
