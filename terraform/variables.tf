###########
### AWS ###
###########

variable "aws_sm_secret_name" {
  type        = string
  description = "AWS Secrets Manager secret name."

  default = "img_to_caption"
}

variable "aws_sm_secret_name_keys" {
  type        = string
  description = "AWS Secrets Manager secret name."

  default = "img_to_caption_keys"
}

variable "aws_vpc_id" {
  type        = string
  description = "Target AWS VPC id."

  default = "vpc-0439a28907e3db7a3"
}

/* EC2 Main */

variable "aws_subnet_id" {
  type        = string
  description = "Target AWS subnet id"

  default = "subnet-0e7e8a95af291ba5d" // us-east-1a private EKS
}

variable "aws_ec2_private_ip" {
  type        = string
  description = "Target AWS EC2 private IP address."

  default = "10.2.170.172"
}

variable "aws_ec2_ami" {
  type        = string
  description = "Target AWS EC2 AMI."

  // TODO move this to dynamic query for latest image
  default = "ami-0044130ca185d0880" // Ubuntu 22.04
}

variable "aws_ec2_instance_type" {
  type        = string
  description = "EC2 instance type slug."

  default = "g5.xlarge" // A10G GPU, 4 vCPU, 16 GB RAM
}

variable "aws_ec2_root_volume_size" {
  type        = number
  description = "Size in GB for EC2 root volume."

  default = 100
}

/* EC2 Bastion */

variable "aws_bastion_subnet_id" {
  type        = string
  description = "Target AWS subnet id"

  default = "subnet-08e09bdd64528801a" // us-east-1a public EKS
}

variable "aws_ec2_bastion_ami" {
  type        = string
  description = "Target AWS EC2 AMI."

  // TODO move this to dynamic query for latest image
  default = "ami-0044130ca185d0880" // Ubuntu 22.04
}

variable "aws_ec2_bastion_instance_type" {
  type        = string
  description = "EC2 instance type slug."

  default = "t3.nano" // 2 vCPU, 0.5 GB RAM
}

variable "aws_ec2_bastion_root_volume_size" {
  type        = number
  description = "Size in GB for EC2 root volume."

  default = 8
}

variable "allow_ssh_from_ip" {
  type        = string
  description = "IP address to allow SSH access from."
}

######################
### Docker Compose ###
######################

variable "image_repo" {
  type    = string
  default = "registry.gitlab.com/minds/data-science/img-to-caption/etl-captioner"
}

variable "image_tag" {
  type    = string
  default = "latest"
}

variable "docker_compose_destination" {
  type        = string
  description = "Destination path on the instance for rendered Docker Compose stack."

  default = "/home/ubuntu/docker-compose.yml"
}
