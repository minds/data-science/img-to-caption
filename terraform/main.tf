terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.66.1"
    }
    template = {
      source  = "hashicorp/template"
      version = "2.2.0"
    }
  }

  backend "s3" {
    bucket = "minds-terraform"
    region = "us-east-1"
    key    = "data-science/img-to-caption/aws/terraform.tfstate"
  }
}

provider "aws" {
  region = "us-east-1"

  default_tags {
    tags = {
      "minds-com.module"            = "img-to-caption"
      "minds-com.module-group"      = "data-science"
      "minds-com.deployment-method" = "terraform"
      "minds-com.environment"       = "aws"
    }
  }
}

data "aws_caller_identity" "current" {}
data "aws_region" "current" {}