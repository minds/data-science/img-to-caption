#!/bin/bash

echo "Starting user_data.sh"

#
echo "Installing dependencies"

curl -s -L https://nvidia.github.io/nvidia-container-runtime/gpgkey | sudo apt-key add -
distribution=$(. /etc/os-release;echo $ID$VERSION_ID)

curl -s -L https://nvidia.github.io/nvidia-container-runtime/$distribution/nvidia-container-runtime.list |\
sudo tee /etc/apt/sources.list.d/nvidia-container-runtime.list

apt-get update
apt-get install -y docker.io docker-compose awscli jq nvidia-container-runtime nvidia-headless-530 nvidia-utils-530

adduser ubuntu docker
systemctl restart docker

until nvidia-smi; do echo "Attempting to communicate with NVIDIA driver..."; done # Wait for NVIDIA driver

#
echo "Fetching secrets"

aws secretsmanager get-secret-value \
  --region us-east-1 \
  --secret-id img_to_caption | jq  -r .SecretString > /home/ubuntu/.env

aws secretsmanager get-secret-value \
  --region us-east-1 \
  --secret-id img_to_caption_keys | jq  -r .SecretString >> /home/ubuntu/.ssh/authorized_keys # Hard code home folder

chown ubuntu /home/ubuntu/.ssh/authorized_keys
chmod 0600 /home/ubuntu/.ssh/authorized_keys

#
echo "Setting swapfile"
dd if=/dev/zero of=/swapfile bs=512M count=32
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile
echo '/swapfile swap swap defaults 0 0' >> /etc/fstab

#
echo "Starting docker-compose"
cd /home/ubuntu

until [ -f docker-compose.yml ]; do sleep 1s echo "Waiting for stack..."; done

docker-compose up -d