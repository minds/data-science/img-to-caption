#!/bin/bash

echo "Starting user_data.sh"

#
echo "Installing dependencies"

apt-get update
apt-get install -y awscli jq

#
echo "Fetching secrets"

aws secretsmanager get-secret-value \
  --region us-east-1 \
  --secret-id img_to_caption_keys | jq  -r .SecretString >> /home/ubuntu/.ssh/authorized_keys # Hard code home folder

chown ubuntu /home/ubuntu/.ssh/authorized_keys
chmod 0600 /home/ubuntu/.ssh/authorized_keys
