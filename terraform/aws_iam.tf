###########
### IAM ###
###########

resource "aws_iam_role" "img_to_caption" {
  name = "img_to_caption"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
}

resource "aws_iam_role_policy" "img_to_caption" {
  name = "secret_access_policy"
  role = aws_iam_role.img_to_caption.id
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "secretsmanager:GetSecretValue"
        Effect = "Allow"
        Resource = [
          "arn:aws:secretsmanager:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:secret:${var.aws_sm_secret_name}-*",
          "arn:aws:secretsmanager:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:secret:${var.aws_sm_secret_name_keys}-*",
        ]
      },
      {
        Action = [
          "s3:ListBucket",
          "s3:ListObjects",
          "s3:GetObject"
        ]
        Effect = "Allow"
        Resource = [
          "arn:aws:s3:::${aws_s3_bucket.ai_models.id}",
          "arn:aws:s3:::${aws_s3_bucket.ai_models.id}/*"
        ]
      }
    ]
  })
}

resource "aws_iam_instance_profile" "img_to_caption" {
  name = "img_to_caption"
  role = aws_iam_role.img_to_caption.name
}
