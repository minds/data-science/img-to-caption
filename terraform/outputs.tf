###########
### AWS ###
###########

output "elastic_ip_address_bastion" {
  value       = aws_eip.bastion.public_ip
  description = "The public IP address of the bastion EC2 instance."
}

output "ec2_ip_address" {
  value       = aws_instance.this.private_ip
  description = "The public IP address of the main EC2 instance."
}
