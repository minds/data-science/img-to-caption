######################
### Docker Compose ###
######################

/**
 * 1. Create a Docker Compose stack using the template located in etc/docker-compose.yml
 * 2. Trigger a replacement of the EC2 instance when the image tag changes.
*/

data "template_file" "docker_compose_stack" {
  template = file("${path.module}/etc/docker-compose.yml")
  vars = {
    image_tag = "${var.image_repo}:${var.image_tag}"
  }
}

resource "terraform_data" "docker_compose_stack" {
  triggers_replace = [var.image_tag]
  input            = data.template_file.docker_compose_stack.rendered
}