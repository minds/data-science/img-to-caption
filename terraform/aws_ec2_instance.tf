####################
### EC2 Instance ###
####################

resource "aws_instance" "this" {

  // In addition to default_tags
  tags = { Name = "img-to-caption-${var.image_tag}" }

  // Compute
  ami                  = var.aws_ec2_ami
  instance_type        = var.aws_ec2_instance_type
  iam_instance_profile = aws_iam_instance_profile.img_to_caption.name
  user_data            = file("${path.module}/etc/user_data.sh")

  root_block_device { volume_size = var.aws_ec2_root_volume_size /* GB */ }

  private_ip = var.aws_ec2_private_ip

  // Network
  vpc_security_group_ids = [aws_security_group.this.id]
  subnet_id              = var.aws_subnet_id

  // Deploy Docker Compose stack
  provisioner "file" {
    content     = data.template_file.docker_compose_stack.rendered
    destination = var.docker_compose_destination
    connection {
      type  = "ssh"
      bastion_user = "ubuntu"
      bastion_host = aws_eip.bastion.public_ip
      user  = "ubuntu"
      host  = self.private_dns
      agent = true // Use local SSH agent
    }
  }

  // Wait for cloud-init to finish
  provisioner "remote-exec" {
    inline = ["cloud-init status --wait"]
    connection {
      type  = "ssh"
      bastion_user = "ubuntu"
      bastion_host = aws_eip.bastion.public_ip
      user  = "ubuntu"
      host  = self.private_dns
      agent = true // Use local SSH agent
    }
  }

  lifecycle {
    create_before_destroy = true                                  // Create new instance prior to deleting the old
    replace_triggered_by  = [terraform_data.docker_compose_stack] // Replace instance when Docker Compose stack changes
  }
}

##############
### AWS SG ###
##############

resource "aws_security_group" "this" {
  name   = "img-to-caption"
  vpc_id = var.aws_vpc_id

  ingress {
    description     = "SSH"
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = [aws_security_group.bastion.id]
  }

  ingress {
    description = "Node Exporter"
    from_port   = 9100
    to_port     = 9100
    protocol    = "tcp"
    cidr_blocks = ["10.7.0.0/16"]
    # cidr_blocks = ["${var.oci_nat_gateway_ip}/32"]
  }

  ingress {
    description = "DCGM Exporter"
    from_port   = 9400
    to_port     = 9400
    protocol    = "tcp"
    cidr_blocks = ["10.7.0.0/16"]
    # cidr_blocks = ["${var.oci_nat_gateway_ip}/32"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

