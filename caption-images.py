# pip install cassandra-driver
from cassandra.cluster import Cluster
from cassandra.auth import PlainTextAuthProvider
from cassandra.policies import WhiteListRoundRobinPolicy
from cassandra.concurrent import execute_concurrent_with_args
import logging
# pip install pulsar-client
import pulsar
from pulsar import schema as pschema
import json
import re
import requests
from os import environ as env
from datetime import datetime
from time import sleep
from collections import deque

# pip install pillow
from PIL import Image
# pip install accelerate git+https://github.com/huggingface/transformers.git -U 
# mamba install -c huggingface accelerate transformers
from transformers import BlipProcessor, Blip2ForConditionalGeneration
from transformers import LlamaConfig, LlamaTokenizer, LlamaForCausalLM, modeling_utils
import transformers
import torch

# pip install boto3
import boto3
from os import system



CASSANDRA_USER = env['PREFECT__CONTEXT__SECRETS__CASSANDRA_USER']
CASSANDRA_PASSWORD = env['PREFECT__CONTEXT__SECRETS__CASSANDRA_PASSWORD']
CASSANDRA_HOST = env['PREFECT__CONTEXT__SECRETS__CASSANDRA_HOST']

PULSAR_HOST = env['PREFECT__CONTEXT__SECRETS__PULSAR_HOST']
PULSAR_ROOT = env['PREFECT__CONTEXT__SECRETS__PULSAR_ROOT']

MODEL_PATH = '/topic_model'
def init_trained_model():
    s3 = boto3.resource('s3')
    s3.meta.client.download_file('minds-ai-models', 'topic_model.tar', '/topic_model.tar')
    system('tar xf /topic_model.tar')


def get_pulsar_client():
    client = pulsar.Client(
        f"pulsar+ssl://{PULSAR_HOST}:6651/",
        tls_trust_certs_file_path='/home/ubuntu/minds-ca.crt',
        tls_allow_insecure_connection=True
    )
    return client

def get_pulsar_consumer():
    consumer = get_pulsar_client().subscribe(f"{PULSAR_ROOT}/entities-ops", 'entities-caption-prod')
    return consumer

def get_caption_pulsar_producer():
    producer = get_pulsar_client().create_producer(topic=f"{PULSAR_ROOT}/captioned-activities", schema=pschema.JsonSchema(ImageCaption))
    return producer

def get_topic_pulsar_producer():
    producer = get_pulsar_client().create_producer(topic=f"{PULSAR_ROOT}/inferred-tags", schema=pschema.JsonSchema(InferredTags))
    return producer

class ImageCaption(pschema.Record):
    activity_urn = pschema.String()
    guid = pschema.Long()
    type = pschema.String()
    container_guid = pschema.Long()
    owner_guid = pschema.Long()
    access_id = pschema.Long()
    time_published = pschema.String()
    time_created = pschema.String()
    tags = pschema.String()
    message = pschema.String()
    caption = pschema.String()

    def dump(self):
        dump_string = f"""{{
    "activity_urn": "{self.activity_urn}",
    "guid": "{self.guid}",
    "type": "{self.type}",
    "container_guid": "{self.container_guid}",
    "owner_guid": "{self.owner_guid}",
    "access_id": "{self.access_id}",
    "time_published": "{self.time_published}",
    "time_created": "{self.time_created}",
    "tags": "{self.tags}",
    "message": "{self.message}",
    "caption": "{self.caption}"
}}"""
        return dump_string

class InferredTags(pschema.Record):
    activity_urn = pschema.String()
    guid = pschema.Long()
    embed_string = pschema.String()
    inferred_tags = pschema.String() # JSON array of strings, e.g. `["one", "two"]`

    def dump(self):
        dump_string = f"""{{
    "activity_urn": "{self.activity_urn}",
    "guid": "{self.guid}",
    "embed_string": "{self.embed_string}",
    "inferred_tags": "{self.inferred_tags}"
}}"""
        return dump_string


class ImageAttachment():
    def __init__(self, guid, type):
        self.guid = guid
        self.type = type
        self.tstamp = datetime.now().timestamp()
        self.retry = 0
        self.image = None

def php_true(value):
    if value in [False, 0, 0.0, -0.0, '', '0', [], None]:
        return False
    else:
        return True

def rectify_caption(text):
    clean = text.strip()
    clean = clean.replace('; the image is a sexy image', '; ')
    clean = clean.replace('; the image is a sex image', '; ')
    clean = clean.replace('; the image is a sex symbol', '; ')
    clean = clean.replace(' is a sexy image', '')
    clean = clean.replace(' is a sex image', '')
    clean = clean.replace(' is a sex symbol', '')
    clean = clean.replace('the image is offensive because ', '')
    clean = clean.replace('it is offensive because ', '')
    return clean

def get_pulsar_ids(logger):
    consumer = get_pulsar_consumer()
    entity_count = 0
    last_timestamp = None
    while True:
        try:
            msg = consumer.receive(timeout_millis=5000)
            body = json.loads(msg.value())
            publish_ts = datetime.fromtimestamp(msg.publish_timestamp()/1000.0).strftime('%Y-%m-%d %H:%M:%S.%f')[0:-3]
            op = body['op']
            urn = body['entity_urn'].split(':')
            entity_type = urn[1]
            entity_guid = urn[2]
            if op == 'create' and entity_type == 'activity':
                last_timestamp = publish_ts
                logger.debug(f"Yielding activity guid {entity_guid} ({body['entity_urn']}), published {str(last_timestamp)}")
                yield (entity_guid, str(publish_ts))
            consumer.acknowledge(msg)
            entity_count += 1
        except:
            consumer.close()
            consumer = get_pulsar_consumer()
        if entity_count % 1000 == 0:
            logger.info(f"Yielded {entity_count} entity ids from pulsar stream, up to {str(last_timestamp)}")

def cassandra_values(key_list, logger):
    logger.info(f"Extracting records for keys")
    auth_provider = PlainTextAuthProvider(username=CASSANDRA_USER, password=CASSANDRA_PASSWORD)
    cluster = Cluster([CASSANDRA_HOST], load_balancing_policy=WhiteListRoundRobinPolicy([CASSANDRA_HOST]), auth_provider=auth_provider)
    cass = cluster.connect()
    stmt = cass.prepare("select * from minds.entities where key = ?")
    err_counter = 0
    for key, time_published in key_list:
        if key != None and key != '':
            try:
                entity = {'key': key}
                for row in cass.execute(stmt, (key,)):
                    entity[row[1]] = row[2]
                if 'deleted' in entity and entity['deleted'] in ['1', 'yes', 't', 'true']:
                    continue
                entity['time_published'] = time_published
                if not 'time_created' in entity:
                    logger.debug(f"Entity {key} does not have 'time_created' attribute, skipping...")
                    continue
                yield entity
            except Exception as e:
                logger.warning(f"Failed to load: select * from minds.entities where key = {key}")
                logger.warning(f"Exception: {repr(e)}")
                err_counter += 1
                if err_counter > 100:
                    return

def extract_activities(logger):
    for activity in cassandra_values(get_pulsar_ids(logger), logger):
        if 'wire_threshold' in activity:
            continue # skip Minds+ activities
                                           
        if 'remind_object' in activity:
            try:
                remind = json.loads(activity['remind_object'])
            except Exception as e:
                logger.warning(f"Failed to parse remind_object value for entity {activity['key']}")
                logger.warning(f"Exception: {repr(e)}")
                continue
            if 'quoted_post' in remind and not php_true(remind['quoted_post']):
                continue # skip "simple shares"

        if 'tags' in activity:
            tags = ' '.join(json.loads(activity['tags']))
        else:
            tags = ''

        activity_urn = f"urn:activity:{activity['key']}"

        attachment_list = []
        if 'attachments' in activity:
            attachments = json.loads(activity['attachments'])
            for attachment in attachments:
                if attachment['type'] == 'image' or attachment['type'] == 'video':
                    attachment_list.append(ImageAttachment(attachment['guid'], attachment['type']))

        yield {
            'activity_urn': activity_urn,
            'guid': activity['key'],
            'type': 'activity',
            'container_guid': activity['container_guid'] if 'container_guid' in activity else '',
            'owner_guid': activity['owner_guid'] if 'owner_guid' in activity else '',
            'access_id': activity['access_id'] if 'access_id' in activity else '', 
            'time_published': activity['time_published'],
            'time_created': datetime.fromtimestamp(int(activity['time_created'])).strftime('%Y-%m-%d %H:%M:%S'),
            'tags': tags,
            'message': activity['message'] if 'message' in activity else '',
            'title': activity['title'] if 'title' in activity else '',
            'blurb': activity['blurb'] if 'blurb' in activity else '',
            'attachments': attachment_list
        }

def load_images(logger):
    retry_interval = 120.0
    retry_count = 5
    dq = deque()
    activities = extract_activities(logger)
    while True:
        now = datetime.now().timestamp()
        activity = None

        try:
            activity = dq.popleft()
            for attachment in activity['attachments']:
                if attachment.image == None and now - attachment.tstamp < retry_interval:
                    dq.appendleft(activity)
                    activity = None
                    break
        except:
            pass
        else:
            if activity != None:
                logger.debug(f"Retrying activity {activity['activity_urn']} from retry queue")

        if activity == None:
            try:
                activity = activities.__next__()
            except:
                pass

        if activity == None:
            if len(dq) > 0:
                sleep(1)
                continue
            else:
                break
        
        for attachment in activity['attachments']:
            if attachment.image != None:
                continue
            elif attachment.retry > retry_count:
                logger.warning(f"Failed to load {attachment.type}:{attachment.guid}")
                continue
            elif attachment.type == 'image':
                image_url = f"https://cdn.minds.com/fs/v1/thumbnail/{attachment.guid}/master"
                logger.debug(f"Loading image thumbnail {image_url}")
            elif attachment.type == 'video':
                desc_url = f"https://www.minds.com/api/v2/media/video/{attachment.guid}"
                logger.debug(f"Loading thumbnail url from {desc_url} for {activity['activity_urn']}")
                try:
                    desc_json = requests.get(desc_url).json()
                    image_url = desc_json['entity']['thumbnail_src']
                    logger.debug(f"Loading video thumbnail {image_url}")
                except:
                    logger.debug(f"Failed to load video thumbnail from {desc_url} for {activity['activity_urn']}")
                    attachment.tstamp = datetime.now().timestamp()
                    attachment.retry += 1
                    dq.append(activity)
                    break
            else:
                continue
            try:
                attachment.image = Image.open(requests.get(image_url, stream=True).raw).convert('RGB')

            except:
                logger.debug(f"Failed to load image {image_url} for activity {activity['activity_urn']}, queuing for retry")
                attachment.tstamp = datetime.now().timestamp()
                attachment.retry += 1
                dq.append(activity)
                break
        else:
            yield activity
                
def caption_images(logger):
    model_name = "ethzanalytics/blip2-flan-t5-xl-sharded"
    processor = BlipProcessor.from_pretrained(model_name)
    model = Blip2ForConditionalGeneration.from_pretrained(model_name, torch_dtype=torch.float16)
    device = "cuda" if torch.cuda.is_available() else "cpu"
    model.to(device)

    prompts = [
        '',
        'Question: Describe what may be offensive about this image? Answer:',
        'Question: What is the style of this image? Answer:',
    ]

    for activity in load_images(logger):
        for image in activity['attachments']:
            logger.debug(f"Processing image {image.type}:{image.guid} of {activity['activity_urn']} as {image.image}")
            if image.image == None:
                continue
            contexts = []
            for prompt in prompts:
                inputs = processor(image.image, text=prompt, return_tensors="pt").to(device, torch.float16)
                generated_ids = model.generate(**inputs, max_new_tokens=50)
                contexts.append(processor.batch_decode(generated_ids, skip_special_tokens=True)[0].strip())
            caption = '; '.join(contexts)
            caption = rectify_caption(caption)

            logger.debug(f"""Caption: "{caption}" at {datetime.now()}""")
            yield {
                'activity_urn': activity['activity_urn'],
                'guid': image.guid,
                'type': image.type,
                'container_guid': activity['container_guid'],
                'owner_guid': activity['owner_guid'],
                'access_id': activity['access_id'], 
                'time_published': activity['time_published'],
                'time_created': activity['time_created'],
                'tags': activity['tags'],
                'message': activity['message'],
                'title': activity['title'],
                'blurb': activity['blurb'],
                'caption': caption
            }

        yield {
            'activity_urn': activity['activity_urn'],
            'guid': activity['guid'],
            'type': activity['type'],
            'container_guid': activity['container_guid'],
            'owner_guid': activity['owner_guid'],
            'access_id': activity['access_id'], 
            'time_published': activity['time_published'],
            'time_created': activity['time_created'],
            'tags': activity['tags'],
            'message': activity['message'],
            'title': activity['title'],
            'blurb': activity['blurb'],
            'caption': ''
        }


def caption_activities(logger):
    logger.info("Opening caption pulsar producer")
    producer = get_caption_pulsar_producer()

    for entity in caption_images(logger):
        logger.debug(f"{json.dumps(entity, indent=4)}\n")
        message_strings = []
        if entity['message'] > '':
            message_strings.append(entity['message'])
        if entity['title'] > '':
            message_strings.append(f"Title: {entity['title']}")
        if entity['blurb'] > '':
            message_strings.append(f"Blurb: {entity['blurb']}")
        message = '; '.join(message_strings)
        record = ImageCaption(
            activity_urn = entity['activity_urn'],
            guid = int(entity['guid']),
            type = entity['type'],
            container_guid = int(entity['container_guid']),
            owner_guid = int(entity['owner_guid']),
            access_id = int(entity['access_id']),
            time_published = entity['time_published'],
            time_created = entity['time_created'],
            tags = entity['tags'],
            message = message,
            caption = entity['caption']
        )
        logger.debug(f"Sending ImageCaption record:\n{record.dump()}")
        producer.send(record)
        yield record

def aggregate_activities(logger):
    current_activity = ""
    activity_map = {}
    for record in caption_activities(logger):
        activity = record.activity_urn.split(':')[2]
        if current_activity != activity:
            activity_map = {
                'activity_urn': record.activity_urn,
                'activity_guid': activity,
                'url': f"https://minds.com/newsfeed/{activity}",
                'tags': record.tags,
                'text': record.message.replace('\n', ' ').replace('"', "'"),
            }
            current_activity = activity
        elif record.type == 'activity': # Preceded by images to get here, so this record is redundant
            continue
        if record.caption > '':
            try: 
                caption, offensive, style = rectify_caption(record.caption).split(';')
                caption_map = {
                    'caption': caption.strip(),
                    'offensive': offensive.strip(),
                    'style': style.strip()
                }
            except:
                caption_map = {}
        else:
            caption_map = {}
        if record.type == 'activity':
            logger.debug(f"Yielding activity aggregate: {activity_map}")
            yield activity_map
        elif record.type == 'image':
            image_map = {'image_guid': str(record.guid) }
            logger.debug(f"Yielding image aggregate: {{**activity_map, **image_map, **caption_map}}")
            yield {**activity_map, **image_map, **caption_map}

def embed_activities(logger):
    offensive_words = ['nude', 'naked', 'ass', 'breasts', 'tits', 'cock', 'pussy']

    for record in aggregate_activities(logger):
        strings = []
        if 'text' in record  and record['text'] > '':
            strings.append(record['text'])
        if 'tags' in record and record['tags'] > '':
            strings.append(f"Tags: {record['tags']}")
        if 'caption' in record and record['caption'] > '':
            strings.append(f"Caption: {record['caption']}")
        if 'offensive' in record and record['offensive'] > '':
            for word in offensive_words:
                if record['offensive'].find(word) > -1:
                    strings.append(f"Offensive: {record['offensive']}")
                    break
        if 'style' in record and record['style'] > '':
            strings.append(f"Style: {record['style']}")
        
        embedded = '; '.join(strings)
        embed_record = {
            'activity_urn': record['activity_urn'],
            'guid': record['activity_guid'],
            'embed_string': embedded
        }
        logger.debug(f"Yielding embed record: {embed_record}")
        yield embed_record

def process_words(input):
    cutwords = ['i', 'you', 'the', 'a', 'to', 'it', 'not', 'that', 'and', 'of', 'do', 'have', 'what', 'we', 'in', 'get', 'this', 'for', 'your', 'at', 'by', 'if',
                'or', 'were', 'is', 'with', 'on', 'from', 'are', 'be', 'all', "i'm", 'my', 'will', 'how', 'they', 'them', 'as', 'who', 'no', 'was', 'can', "don't",
                'more', 'when', 'an', 'so', 'but', "it's", 'am', 'he', 'her', 'our', "can't", "i've", 'put', 'give', 'style', 'caption', 'topic', 'words']
    words = re.findall(r"""\b[a-z][a-z\']*[a-z]\b""", input.lower().replace('’', "'"))
    sorted_words = sorted(set(words))
    for word in cutwords:
        if word in sorted_words:
            sorted_words.remove(word)

    return json.dumps(sorted_words)

def autotopic_activities():
    logging.basicConfig()
    logger = logging.getLogger(__name__)
    # logger.setLevel(10)
    logger.setLevel(20)

    logger.info("Installing topic inferrence model")
    init_trained_model()
    model_path = '/topic_model'
    tokenizer = LlamaTokenizer.from_pretrained(model_path, legacy=False)
    tokenizer.bos_token_id = 1
    logger.info("Loading topic inferrence model")
    model = LlamaForCausalLM.from_pretrained(
        model_path, torch_dtype=torch.float16, device_map='cuda'
    )
    producer = get_topic_pulsar_producer()
    for embed_record in embed_activities(logger):
        embed = embed_record['embed_string'][:2048]
        prompt = f"""Provide several topic words for the following text:\n{embed}\n\nTopic words:\n"""
        input_ids = tokenizer(prompt, return_tensors="pt").input_ids
        input_ids = input_ids.to('cuda')
        if torch.numel(input_ids) > 2048:
            logger.warn(f"Tokenized tensor exceeds 2048 elements for string, skipping: {prompt}")
            continue

        generation_output = model.generate(input_ids=input_ids, max_new_tokens=50, temperature=0.25, do_sample=True)
        output_string = tokenizer.decode(generation_output[0])
        output_string = output_string.split('\nA:')[0]
        output_string = output_string.split("\nTopic words:\n")[1]
        tag_string = process_words(output_string)

        inferred_record = InferredTags(
            activity_urn = embed_record['activity_urn'],
            guid = int(embed_record['guid']),
            embed_string = embed,
            inferred_tags = tag_string
        )
        logger.debug(f"Sending InferredTags record:\n{inferred_record.dump()}")
        producer.send(inferred_record)

autotopic_activities()
