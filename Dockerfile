FROM nvidia/cuda:12.1.0-base-ubuntu22.04
LABEL maintainer="minds"
LABEL repository="etl-captioner"

RUN apt update && \
    apt install -y bash \
                   build-essential \
                   git \
                   curl \
                   ca-certificates \
                   python3 \
                   python3-pip && \
    rm -rf /var/lib/apt/lists

RUN python3 -m pip install --no-cache-dir --upgrade pip && \
    python3 -m pip install --no-cache-dir \
    jupyter \
    tensorflow \
    torch \
    sentencepiece \
    boto3

RUN pip install accelerate git+https://github.com/huggingface/transformers.git
RUN pip install pulsar-client cassandra-driver pillow

COPY caption-images.py caption-images.py
COPY preload-models.py preload-models.py
COPY run.sh run.sh

CMD ["/bin/bash", "./run.sh"]
