# Image captioner Docker container

## Overview

This repository creates a Docker image for a captioning service that reads image entity ids from a pulsar stream, downloads the respective images from the CDN service, generates a caption for each image and sends the caption back on another pulsar stream.

The captioning is done by a [BLIP-2 model](https://huggingface.co/blog/blip-2#using-blip-2-with-hugging-face-transformers) which requires CUDA GPU support, which in turn requires special Docker setup for this container to run.

## Setup

To set up Docker to run GPU-accelerated containers (on Ubuntu), first install the `nvidia-container-runtime`:

```
$ curl -s -L https://nvidia.github.io/nvidia-container-runtime/gpgkey | sudo apt-key add -
distribution=$(. /etc/os-release;echo $ID$VERSION_ID)

$ curl -s -L https://nvidia.github.io/nvidia-container-runtime/$distribution/nvidia-container-runtime.list |\
sudo tee /etc/apt/sources.list.d/nvidia-container-runtime.list

$ sudo apt-get update

$ sudo apt-get install nvidia-container-runtime
```

Once `nvidia-container-runtime` is installed, restart Docker:

```
$ sudo systemctl stop docker
$ sudo systemctl start docker
```

## Build

To build a docker image from this repo, check out the repository, change into the root directory, and run docker build, specifying the version of the image as appropriate (e.g. `0.0.1`):

```
$ docker build . -t minds/etl-captioner:0.0.1
```

This may take a while, as it will download and install the BLIP-2 model from Hugging Face.

## Configuration

The captioner program requires a copy of the Prefect ETL configuration variables in the environment. This docker image populates the necessary variables in the environment from a `.env` file mounted into the runtime container at `/.env`. The `.env` file template is as follows:

```
export PREFECT__CONTEXT__SECRETS__SNOWFLAKE_ACCOUNT=
export PREFECT__CONTEXT__SECRETS__SNOWFLAKE_USER=
export PREFECT__CONTEXT__SECRETS__SNOWFLAKE_PASSWORD=
export PREFECT__CONTEXT__SECRETS__SNOWFLAKE_ROLE=
export PREFECT__CONTEXT__SECRETS__SNOWFLAKE_WAREHOUSE=
export PREFECT__CONTEXT__SECRETS__SNOWFLAKE_DATABASE=
export PREFECT__CONTEXT__SECRETS__SNOWFLAKE_CASSANDRA_SCHEMA=
export PREFECT__CONTEXT__SECRETS__SNOWFLAKE_VITESS_SCHEMA=
export PREFECT__CONTEXT__SECRETS__SNOWFLAKE_DBT_SCHEMA=
export PREFECT__CONTEXT__SECRETS__SNOWFLAKE_ES_SCHEMA=
export PREFECT__CONTEXT__SECRETS__SNOWFLAKE_SNOWPLOW_SCHEMA=

export PREFECT__CONTEXT__SECRETS__VITESS_HOST=
export PREFECT__CONTEXT__SECRETS__VITESS_PORT=
export PREFECT__CONTEXT__SECRETS__VITESS_SCHEMA=
export PREFECT__CONTEXT__SECRETS__VITESS_USER=
export PREFECT__CONTEXT__SECRETS__VITESS_PASSWORD=

export PREFECT__CONTEXT__SECRETS__PULSAR_HOST=
export PREFECT__CONTEXT__SECRETS__PULSAR_ROOT=

export PREFECT__CONTEXT__SECRETS__CASSANDRA_USER=
export PREFECT__CONTEXT__SECRETS__CASSANDRA_PASSWORD=
export PREFECT__CONTEXT__SECRETS__CASSANDRA_HOST=
export PREFECT__CONTEXT__SECRETS__ES_USER=
export PREFECT__CONTEXT__SECRETS__ES_PASSWORD=
export PREFECT__CONTEXT__SECRETS__ES_HOST=
export PREFECT__CONTEXT__SECRETS__DBT_ENVIRONMENT=
```
Note that any passwords containing special characters will need to be quoted with single quotes.

## Run

To run the image from the Ubuntu command line, enable GPU acceleration, host networking, and mount the configuration file:

```
$ docker run --gpus all --network host --mount type=bind,src="$(pwd)"/.env,dst=/.env -d minds/etl-captioner:0.0.1
```
